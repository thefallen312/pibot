from collections import defaultdict
from datetime import date
import random

from discord import Client, message, user
import redis
from redis.commands.json.path import Path


class PiClient(Client):
    def __init__(self, r: redis.Redis, *args, **kwargs):
        super().__init__(**kwargs)
        self.redis = r
        self.pidors = defaultdict(lambda: ("", date.today()))
        self.p_lists = {}

    def define_pidor(self, msg: message) -> user:
        member_seq = []
        for member in msg.guild.members:
            if member.bot:
                continue
            if self.redis.get(f"{msg.guild.id}:role") and int(
                self.redis.get(f"{msg.guild.id}:role")
            ) not in [role.id for role in member.roles]:
                continue
            member_seq.append(member)

        if self.redis.exists(f"{msg.guild.id}:pidor") and date.today().strftime(
            "%Y-%m-%d"
        ) == self.redis.json().get(msg.guild.id, Path(".date")):
            pidor = self.get_user(
                self.redis.json().get(f"{msg.guild.id}:pidor", Path(".id"))
            )
        else:
            pidor = random.choice(member_seq)
            pidor_dict = {"id": pidor.id, "date": date.today().strftime("%Y-%m-%d")}
            self.redis.json().set(f"{msg.guild.id}:pidor", Path.root_path(), pidor_dict)

        return pidor

    def forget_pidor(self, guild_id: int):
        self.redis.delete(guild_id)

    def set_pidor_role(self, guild_id: int, role_id: int):
        self.redis.set(f"{guild_id}:role", role_id)

    def del_pidor_role(self, guild_id: int):
        self.redis.delete(f"{guild_id}:role")

    async def on_ready(self):
        print(f"{self.user} has connected to Discord!")

    async def on_message(self, msg: message):
        if msg.author == self.user:
            return

        if self.user.id not in msg.raw_mentions:
            return

        if "Кто пидор" in msg.content:
            try:
                await msg.channel.send(
                    f"{self.define_pidor(msg).mention} Ты - пидор дня!"
                )
            except IndexError:
                await msg.channel.send(
                    f"Ни одного пользователя с ролью {msg.guild.get_role(int(self.redis.get(f'{msg.guild.id}:role'))).mention}"
                )

        if "Забудь пидора" in msg.content:
            self.forget_pidor(msg.guild.id)
            await msg.channel.send(f"Давай по новой!")

        if "Запомни роль пидора" in msg.content:
            self.set_pidor_role(msg.guild.id, msg.role_mentions[0].id)
            await msg.channel.send("Запомнил!")

        if "Забудь роль пидора" in msg.content:
            self.del_pidor_role(msg.guild.id)
            await msg.channel.send("Забыл!")
