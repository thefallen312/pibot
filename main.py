import discord
import redis

from pidor_clients import PiClient


redis_client = redis.Redis(decode_responses=True)

intents = discord.Intents.default()
intents.members = True

client = PiClient(redis_client, intents=intents)

client.run(str(redis_client.get("PiBot_AUTH_KEY")))
